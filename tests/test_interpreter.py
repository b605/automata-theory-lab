import unittest

from core.grammar import Grammar
from core.rpn import TableRPN
from core.lexer import ComplexLexer, PlyLexer
from core.interpreter import Interpreter
from core.exc import InterpreterException

from settings import Config

ID = 1


class TestInterpreter(unittest.TestCase):
    @staticmethod
    def assign_id():
        global ID
        print(ID)
        ID += 1
        return f"_{ID - 1}"

    def setUp(self) -> None:
        # Setup analyzer, rnp and grammar
        self.grammar = Grammar.parse_from_file(Config.INPUT_NORMAL_RULES_PATH, "S")
        self.rpn_table = TableRPN.from_file(self.grammar, Config.INPUT_RPN_RULES_PATH)
        self.grammar.generate_table()
        self.lexer = PlyLexer
        self.interpreter = Interpreter(self.grammar, self.rpn_table, lexer=self.lexer)

    def test_conditional_1(self):

        code = """
                int i, a;
                {   
                    i = 100;
                    if (i > 50) {
                        write(i*5);
                    }
                    else {
                        if ( i <= 50) {
                            write(i - 25);
                        }
                        else {
                            write(i);
                        }
                    }
                }
                """
        try:
            result = self.interpreter.run(code, [], False)
            self.assertEqual([500], result)
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

    def test_loop_1(self):
        code = """
                int i;
                {   
                    i = 100;
                    while (i > 95) {
                        i = i - 2;
                        write(i);
                    }
                }
                """

        try:
            result = self.interpreter.run(code, [], False)
            self.assertEqual([98, 96, 94], result)
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

    def test_loop_2(self):
        code = """
                int i, a;
                {   
                    i = 1;
                    a = 500;
                    while (i < a) {
                        i = (i + 2) * 3;
                        write(i);
                    }
                }
                """

        try:
            result = self.interpreter.run(code, [], False)
            self.assertEqual([9, 33, 105, 321, 969], result)
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

    def test_loop_3(self):
        code = """
                int i, a;
                {   
                    i = 1;
                    a = 500;
                    if (a < i) {
                        a = i;
                    }
                    else {
                        while (i < a) {
                            i = (i + 2) * 3;
                            a = a + 1;
                            write(i);
                        }
                    }
                }
                """

        try:
            result = self.interpreter.run(code, [], False)
            self.assertEqual([9, 33, 105, 321, 969], result)
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

    def test_loop_4(self):
        code = """
                int i, a;
                {   
                    i = 1;
                    a = 500;
                    while(a > i) {
                        a = a - 2;
                        i = i*2;
                        write(i);
                    }
                }
                """

        try:
            result = self.interpreter.run(code, [], False)
            self.assertEqual(9, len(result))
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

    def test_array_1(self):
        code = """
                int i, n;
                array A;
                {   
                    i = 0;
                    n = 10;
                    make(A, n);
                    while(n > i) {
                        A[i] = i*i;
                        write(A[i]);
                        i = i + 1;
                    }
                }
                """
        try:
            result = self.interpreter.run(code, [], False)
            self.assertEqual([0, 1, 4, 9, 16, 25, 36, 49, 64, 81], result)
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

    def test_array_read(self):

        code = """
                int i, n;
                array A;
                {   
                    i = 0;
                    read( n );
                    make(A, n);
                    while(n > i) {
                        A[i] = i*2;
                        write(A[i]);
                        i = i + 1;
                    }
                }
                """

        try:
            result = self.interpreter.run(code, [4], False)
            self.assertEqual([0, 2, 4, 6], result)
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

    def test_math(self):
        code = """
                int a, b, c;
                {
                    a = -10;
                    b = -50;
                    c = abs(a) * sqrt(sqrt(11 + abs(a * 2 + b)) * 4);
                    write(c);
                }
                """

        try:
            result = self.interpreter.run(code, [], False)
            self.assertEqual([60], result)
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

        code = """
                int a, b;
                {
                    a = 10;
                    b = sqrt(a);
                }
                """

        try:
            self.interpreter.run(code)
        except InterpreterException as e:
            self.fail(f"Unexpected InterpreterException: {e}")

    def test_complex_sort_1(self):
        code = """
                int i,j,x,n;
                array A;
                {   
                    read( n );
                    make(A, n);
                    
                    i = 0;
                    while(n > i) {
                        read(A[i]);
                        i = i + 1;
                    }
                    
                    i = 0;
                    j = 0;
                    while(n-1 > i) {
                        
                        j = i + 1;
                        while(n > j) {
                        
                            if (A[j] < A[i]) {
                                x = A[j];
                                A[j] = A[i];
                                A[i] = x;
                            }
                        
                            j = j + 1;
                        }
                        
                        i = i + 1;
                    }
                    
                    i = 0;
                    while(n > i) {
                        write(A[i]);
                        i = i + 1;
                    }
                }
                """

        try:
            result = self.interpreter.run(code, [6, 3, 4, 5, 2, 1, 6], False)
            self.assertEqual([1, 2, 3, 4, 5, 6], result)
        except InterpreterException as e:
            self.fail(f"InterpreterException: {e}")

    def test_exceptions(self):
        code = """
                int i,j,x,n;
                array A;
                {   
                    make(A, -5);
                }
                """

        try:
            self.interpreter.run(code)
            self.fail(f"expected InterpreterException: negative allocation size")
        except InterpreterException:
            pass

        code = """
                int i,j,x,n;
                array A;
                {   
                    make(A, 0);
                }
                """

        try:
            self.interpreter.run(code)
            self.fail(f"expected InterpreterException: allocation zero elements")
        except InterpreterException:
            pass

        code = """
                int i,j,x,n;
                {   
                    i = j;
                    write(i+5);
                }
                """

        try:
            self.interpreter.run(code, [])
            self.fail(f"expected InterpreterException: assign undefined variable")
        except InterpreterException:
            pass

        code = """
                int i,j,x,n;
                {   
                    write(i);
                }
                """

        try:
            self.interpreter.run(code, [])
            self.fail(f"expected InterpreterException: undefined variable")
        except InterpreterException:
            pass

        code = """
                int i,j,x,n;
                array A;
                {   
                    make(A, 10);
                    read(A[10]);
                }
                """

        try:
            self.interpreter.run(code, [12])
            self.fail(f"expected InterpreterException: wrong index")
        except InterpreterException:
            pass

        code = """
                int i,j,x,n;
                array A;
                {   
                    make(A, 10);
                    read(A[-5]);
                }
                """

        try:
            self.interpreter.run(code, [12])
            self.fail(f"expected InterpreterException: wrong index")
        except InterpreterException:
            pass

        code = """
                int x;
                {   
                    x = 10/2;
                    x = x/0;
                }
                """

        try:
            self.interpreter.run(code, [12])
            self.fail(f"expected InterpreterException: ZeroDivision")
        except InterpreterException:
            pass

        code = """
                int a;
                {
                    a = sqrt(-100);
                }
                """

        try:
            self.interpreter.run(code)
            self.fail(f"expected InterpreterException: negative sqrt argument")
        except InterpreterException as e:
            pass
