from typing import Optional
import unittest
from pathlib import Path
from core.analyzer import Analyzer
from core.exc import AnalyzerException, GeneratorException
from core.grammar import Grammar
from core.lexer import PlyLexer
from core.rpn import TableRPN
from settings import Config


class TestAnalyzer(unittest.TestCase):
    def setUp(self) -> None:
        # Setup analyzer, rnp and grammar
        self.grammar = Grammar.parse_from_file(Config.INPUT_NORMAL_RULES_PATH, "S")
        self.rpn_table = TableRPN.from_file(self.grammar, Config.INPUT_RPN_RULES_PATH)
        self.grammar.generate_table()
        self.analyzer = Analyzer(self.grammar, self.rpn_table, lexer=PlyLexer)

        self.result_filename = "analytics__{}"

    def get_out_path(self, description: Optional[str] = None) -> Path:
        if not description:
            description = "analytics"
        return Config.OUTPUT_DIR / (self.result_filename.format(description))

    def test_correct_rules(self):
        for noterminal in self.grammar.noterminals:
            for terminal, rpn_rules in self.rpn_table[noterminal].items():
                self.assertEqual(len(rpn_rules), len(self.grammar.table[noterminal][terminal]))

    def test_conditions(self):

        code = """
                int a, b, c;
                { 
                    if (5 > 2) {
                        a = b;
                        c = a + b;
                        a = c * (b/a);
                    }
                    else {
                        b = a;
                    }
                }
                """

        try:
            self.analyzer.run(code, self.get_out_path("conditions"))
        except GeneratorException as e:
            self.fail(f"GeneratorException: {e}")
        except AnalyzerException as e:
            self.fail(f"AnalyzerException: {e}")

    def test_loop(self):
        code = """
                int i;
                {   
                    i = 100;
                    while (i < 10) {
                        i = i - 1;
                    }
                }
                """

        try:
            self.analyzer.run(code, self.get_out_path("loop"))
        except GeneratorException as e:
            self.fail(f"GeneratorException: {e}")
        except AnalyzerException as e:
            self.fail(f"AnalyzerException: {e}")

    def test_complex(self):
        code = """
                int i, y, x, n;
                array RER;
                {   
                    i = 100;
                    make (RER, i);
                    read(RER[i-1]);
                    read(y);
                    n = i - i + i / y;
                    while (i < 10) {
                        i = i - 1;
                        if (i > 50) {
                            write(i+5*4);
                            read(y);
                            write(y+5);
                        }
                        else {
                            write(-5555);
                        }
                    }
                }
                """

        try:
            self.analyzer.run(code, self.get_out_path("complex"))
        except GeneratorException as e:
            self.fail(f"GeneratorException: {e}")
        except AnalyzerException as e:
            self.fail(f"AnalyzerException: {e}")

    def test_math(self):
        code = """
                int x, y, k;
                {
                    x = 20;
                    y = -3;
                    k = abs(sqrt(-100) + 2 * x - 3) * y / (sqrt(22 + x) - 100);
                }
                """

        try:
            self.analyzer.run(code, self.get_out_path("math"))
        except GeneratorException as e:
            self.fail(f"GeneratorException: {e}")
        except AnalyzerException as e:
            self.fail(f"AnalyzerException: {e}")

    def test_exceptions(self):
        code = """
                int i;
                {   
                    j = 5;
                }
                """

        try:
            self.analyzer.run(code, self.get_out_path("exc1"))
            self.fail(f"expected Undefined Variable GeneratorException")
        except GeneratorException as e:
            pass
        except AnalyzerException as e:
            self.fail(f"expected Undefined Variable GeneratorException")

        code = """
                int i;
                {   
                    while ) {

                    }
                }
                """

        try:
            self.analyzer.run(code, self.get_out_path("exc2"))
            self.fail(f"expected  AnalyzerException")
        except GeneratorException as e:
            self.fail(f"expected  AnalyzerException")
        except AnalyzerException as e:
            pass

        code = """
                int i y;
                {   
                    while () {

                    }
                }
                """

        try:
            self.analyzer.run(code, self.get_out_path("exc3s"))
            self.fail(f"expected  AnalyzerException")
        except GeneratorException as e:
            self.fail(f"expected  AnalyzerException")
        except AnalyzerException as e:
            pass
