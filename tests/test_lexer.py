import unittest
from core.exc import LexerException
import core.lexer


class TestLexer(unittest.TestCase):
    def setUp(self) -> None:
        self.lexer = core.lexer.ComplexLexer()

    def test_get_tokens_1(self):
        code = """
                int a, b, c;
                { 
                    if (5 > 2) {
                        a = b;
                        c = a + b;
                        a = c * (b/a);
                    }
                    else {
                        if a != b {
                            c = a <= b;
                        }
                        b = a;
                    }
                    while a == b {
                        /*hello
                        worldf*/
                        k = a - b
                        m = a >= b
                        some_function(k, m)
                    }
                }
                """
        tokens = self.lexer.get_tokens(code)
        self.assertEqual(78, len(tokens))
        for token in tokens:
            self.assertEqual(type(token.value), type(token.type))
            self.assertEqual(True, token.type in core.lexer.tokens)

    def test_get_tokens_2(self):
        code = """
                inta, b, c;?
                {

                }
               """
        try:
            tokens = self.lexer.get_tokens(code)
            self.fail("Lexer error: accepted an unidentified symbol")
        except LexerException:
            pass

    def test_keyword_name_identifier(self):
        code = """
                inta, b, c;
                {

                }
               """
        tokens = self.lexer.get_tokens(code)
        self.assertEqual(8, len(tokens))
