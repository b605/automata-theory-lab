import os
from pathlib import Path


class Config:
    """
    Конфигурационный класс
    для определния структуры файлов.
    """

    ROOT_DIR = Path(os.path.abspath(__file__)).parent
    DATA_DIR = ROOT_DIR / "data"
    OUTPUT_DIR = ROOT_DIR / "output"

    INPUT_NORMAL_RULES_PATH = DATA_DIR / "normal_rules.txt"
    INPUT_RPN_RULES_PATH = DATA_DIR / "rpn.txt"

    OUTPUT_LL_TABLE = OUTPUT_DIR / "table.txt"
    OUTPUT_RPN_TABLE = OUTPUT_DIR / "rpn_table.txt"
