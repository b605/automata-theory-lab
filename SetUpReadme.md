# Как начать работать над проектом ?

1. Необходимо создать виртуальную среду (3.9 или выше)

    >python -m venv venv

2. Активируем виртуальную среду (показано для Windows shell). Если не настроенная среда в IDE, нужно активировать ее каждый раз в терминале

    >.\venv\Scripts\activate

3. Далее устанавливаем зависимости проекта

    >pip install poetry  
    >poetry install
    
4. Запуск тестов (по аналогии можно написать другие тесты)

    >python -m unittest tests.test_interpreter  
    >python -m unittest tests.test_analyzer
