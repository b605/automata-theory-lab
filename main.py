from core.grammar import Grammar
import core.rpn as rpn
import core.utility as utility
from settings import Config

if __name__ == "__main__":

    grammar = Grammar.parse_from_file(Config.INPUT_NORMAL_RULES_PATH, "S")

    rpn_table = rpn.TableRPN.from_file(grammar, Config.INPUT_RPN_RULES_PATH)

    grammar.generate_table()

    utility.print_table_ll(
        "output/table.csv", grammar, table_generator=utility.CSVTableGenerator
    )
    utility.print_table_rpn(
        "output/rpn_table.csv",
        rpn_table,
        grammar,
        table_generator=utility.CSVTableGenerator
    )

    # utility.print_table_ll("output/table.txt", grammar)
    # utility.print_table_rpn("output/rpn_table.txt", rpn_table, grammar)
