from enum import Enum
from typing import Any, Optional, Union, List


class Operand:
    """
    Базовый класс для остальных всех операндов
    """

    def __init__(self, value: Any) -> None:
        self.value = value

    def get_value(self, collection: Any) -> int:
        raise NotImplementedError

    def __repr__(self) -> str:
        return f"Operand"

    def __repr__(self) -> str:
        return str(self)


class ArrayReference(Operand):
    """
    Операнд элемента массива.
    Хранит идентификатор массива и указатель на элемент массива
    для доступа к внешней коллекции
    """

    def __init__(self, array: str, value: int) -> None:
        """Ссылка на элемент массива

        :param array: идентификатор массива
        :type array: str
        :param value: индекс элемента в массиве
        :type value: Any
        """
        self.array = array
        super().__init__(value)

    def get_value(self, collection: dict) -> int:
        return collection["array"][self.array][self.value]

    def set_value(self, variables: dict, value: int) -> None:
        variables["array"][self.array][self.value] = value

    def __str__(self) -> str:
        return f"ar:{self.array}:{self.value}"

    def __repr__(self) -> str:
        return str(self)


class IntegerVariable(Operand):
    """
    Операнд целочисленной переменной.
    Хранит идентификатор для доступа к внешней коллекции
    """

    def __init__(self, value: str) -> None:
        """
        :param value: идентификатор переменной во внешней коллекции
        :type value: str
        """
        super().__init__(value)

    def get_value(self, collection: dict) -> int:
        return collection["int"][self.value]

    def set_value(self, collection: dict, value: int):
        collection["int"][self.value] = value

    def __str__(self) -> str:
        return f"v:{self.value}"

    def __repr__(self) -> str:
        return str(self)


class ArrayVariable(Operand):
    """
    операнд массива,  используется в действии индексирования, чтобы получить ArrayReference
    """

    def __init__(self, value: str) -> None:
        """
        :param value: индетификатор переменной массива во внешней коллекции
        :type value: str
        """
        super().__init__(value)

    def length(self, collection: dict) -> Optional[int]:
        if collection["array"][self.value]:
            return len(collection["array"][self.value])
        return None

    def get_array_ref(self, collection: dict, index: int) -> Optional[ArrayReference]:
        """ Возвращает операнд элемента массива по индексу

        :param collection: коллекция массива
        :param index: целое число
        :return: операнд элемента массива
        """
        if 0 <= index and index < self.length(collection):
            return ArrayReference(self.value, index)
        return None

    def make(self, collection: dict, size: int, default: Optional[int] = None):
        collection["array"][self.value] = [default for i in range(size)]

    def __str__(self) -> str:
        return f"A:{self.value}"

    def __repr__(self) -> str:
        return str(self)


class Constant(Operand):
    """
    Операнд константы, содержит указатель значения во внешней коллекции
    """

    def __init__(self, value: int) -> None:
        self.value = value

    def get_value(self, collection: List[int]) -> int:
        return collection[self.value]

    def __str__(self) -> str:
        return f"c:{self.value}"

    def __repr__(self) -> str:
        return str(self)


class Point(Operand):
    """
    Операнд точки перехода, генерируется только перед jf и j.
    Хранит конечное значение перехода
    """

    def __init__(self, value: int) -> None:
        super().__init__(value)

    def get_value(self) -> int:
        return self.value

    def __str__(self) -> str:
        return f"p:{self.value}"

    def __repr__(self) -> str:
        return str(self)


class TempValue(Operand):
    """
    Операнд сгенерированный в процессе интерпретации.
    хранит конечное значение в главном поле value
    """

    def __init__(self, value: Union[float, int]) -> None:
        super().__init__(value)

    def get_value(self) -> Union[float, int]:
        return self.value

    def __str__(self) -> str:
        return f"t:{self.value}"

    def __repr__(self) -> str:
        return str(self)


class Action(Enum):
    """
    Список действий, выполняемых в интерпретаторе
    """

    assign = "="
    jump = "jf"
    unconditional_jump = "j"
    make = "m"
    write = "w"
    read = "r"
    index = "i"
    plus = "+"
    minus = "-"
    divide = "/"
    multiply = "*"
    gt = ">"
    ge = ">="
    lt = "<"
    le = "<="
    eq = "=="
    neq = "!="
    negate = "neg"
    abs = "abs"
    sqrt = "sqrt"


class Element:
    """
    Элемент для интерпретации, содержит в value либо операнд либо действие
    """

    def __init__(self, value: Union[Operand, Action]) -> None:
        self.type = (
            ElementTypes.OPERAND if isinstance(value, Operand) else ElementTypes.ACTION
        )
        self.value = value

    @property
    def is_operand(self):
        return isinstance(self.value, Operand)

    @property
    def is_action(self):
        return not self.is_operand()

    def __str__(self) -> str:
        if isinstance(self.value, Action):
            return str(self.value.value)
        return str(self.value)


class ElementTypes(Enum):
    OPERAND = 1
    ACTION = 2


# Конвертация строки в действие интерпретатора
CONVERT_TO_INTERPRETER_ACTION = {
    "=": Action.assign,
    "jf": Action.jump,
    "j": Action.unconditional_jump,
    "m": Action.make,
    "w": Action.write,
    "r": Action.read,
    "i": Action.index,
    "+": Action.plus,
    "-": Action.minus,
    "/": Action.divide,
    "*": Action.multiply,
    ">": Action.gt,
    ">=": Action.ge,
    "<": Action.lt,
    "<=": Action.le,
    "==": Action.eq,
    "!=": Action.neq,
    "neg": Action.negate,
    "abs": Action.abs,
    "sqrt": Action.sqrt
}
