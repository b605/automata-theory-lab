import argparse
import os

from core.grammar import Grammar
from core.rpn import TableRPN
from core.interpreter import Interpreter
from settings import Config

parser = argparse.ArgumentParser(description="Study language interpreter")

parser.add_argument("--file", "-f", type=str)

if __name__ == "__main__":
    namespace = parser.parse_args()

    code = ""
    with open(namespace.file, "r") as f:
        for line in f.readlines():
            code += line

    grammar = Grammar.parse_from_file(Config.INPUT_NORMAL_RULES_PATH, "S")
    rpn_table = TableRPN.from_file(grammar, Config.INPUT_RPN_RULES_PATH)
    grammar.generate_table()
    interpreter = Interpreter(grammar, rpn_table)

    interpreter.run(code, None, True)
