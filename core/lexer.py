import ply.lex
from typing import List
from core.exc import LexerException
from abc import ABC


# type used in Interpreter, Generator and Analyzer
class Token:
    def __init__(self, type, value) -> None:
        self.type = type
        self.value = value

    def __repr__(self) -> str:
        return f"Token(type='{self.type}',value='{self.value}')"


class Lexer(ABC):
    """
    Абстрактный класс лексического анализатора
    """
    def __init__(self) -> None:
        pass

    def get_tokens(self, data: str) -> List[Token]:
        """Парсит текст на токены программы

        :param data: Входные данные
        :return: Список токенов
        """
        pass

    @staticmethod
    def tokenize(token: Token) -> str:
        pass


tokens = ("KEYWORD", "VARIABLE_NAME", "INTEGER")

single_symbols = ("-", "*", "+", "(", ")", "{", "}", "[", "]", ",", ";", "~")
other_symbols = ("=", ">", "<", "/", "==", ">=", "<=", "!=", "/*", "*/")
keywords = (
    "int",
    "array",
    "if",
    "else",
    "while",
    "write",
    "read",
    "make",
    "abs",
    "sqrt",
)


class ComplexLexer(Lexer):
    ch_counter = 0
    sym_in_line = 0
    line_pos = 0
    is_comment = False

    def _get_number(self, token: Token):
        intval = 0
        while self.ch.isdigit():
            intval = intval * 10 + int(self.ch)
            self._get_c()
        token.type = "INTEGER"
        token.value = str(intval)

    def _get_composite_symbol(self, sym: str, token: Token):
        if sym == "/":
            if self.ch == "*":
                self.is_comment = True
                self._get_c()
            else:
                token.type = "KEYWORD"
                token.value = sym
        elif sym == "*":
            if self.ch == "/":
                self.is_comment = False
                self._get_c()
        elif self.ch == "=":
            token.type = "KEYWORD"
            token.value = sym + self.ch
            self._get_c()
        elif sym == "!":
            raise LexerException("Unknown identifier: " + self.ch)
        else:
            token.type = "KEYWORD"
            token.value = sym

    def _get_word(self, token: Token):
        word = ""
        while (self.ch.isalpha()) or (self.ch.isdigit()) or (self.ch == "_"):
            word += self.ch
            self._get_c()
        if word in keywords:
            token.type = "KEYWORD"
            token.value = word
        else:
            token.type = "VARIABLE_NAME"
            token.value = word

    def _get_c(self):
        if self.ch_counter < len(self.code):
            self.ch = self.code[self.ch_counter]
            self.ch_counter += 1
            self.sym_in_line += 1
        else:
            self.ch = ""

    def _get_token(self) -> Token:
        token = Token(None, None)
        while token.type == None:
            if len(self.ch) == 0:
                return token
            elif self.ch == "\n":
                self._get_c()
                self.line_pos += 1
                self.sym_in_line = 0
            elif self.is_comment == True:
                if self.ch == "*":
                    sym = self.ch
                    self._get_c()
                    self._get_composite_symbol(sym, token)
                else:
                    self._get_c()
            elif self.ch.isspace():
                self._get_c()
            elif self.ch.isdigit():
                self._get_number(token)
            elif self.ch in single_symbols:
                token.value = self.ch
                token.type = "KEYWORD"
                self._get_c()
            elif self.ch == "=":
                sym = self.ch
                self._get_c()
                self._get_composite_symbol(sym, token)
            elif self.ch == "<":
                sym = self.ch
                self._get_c()
                self._get_composite_symbol(sym, token)
            elif self.ch == ">":
                sym = self.ch
                self._get_c()
                self._get_composite_symbol(sym, token)
            elif self.ch == "!":
                sym = self.ch
                self._get_c()
                self._get_composite_symbol(sym, token)
            elif (self.ch.isalpha()) or (self.ch == "_"):
                self._get_word(token)
            elif self.ch == "/":
                sym = self.ch
                self._get_c()
                self._get_composite_symbol(sym, token)
            else:
                raise LexerException("Unexpected symbol: " + self.ch)
        return token

    def get_tokens(self, data: str) -> List[Token]:
        """Парсит текст на токены программы

        :param data: Входные данные
        :return: Список токенов
        """
        self.code = data
        result = []
        self._get_c()
        token = self._get_token()
        while token.type != None:
            result.append(Token(token.type, token.value))
            token = self._get_token()
        return result


def t_error(t):
    raise LexerException(f"Illegal character '{t.value[0]}'")


t_KEYWORD = r"int|array|if|else|while|write|read|make|abs|sqrt|;|==|>=|<=|!=|=|>|<|/|\*|-|\+|\(|\)|\{|\}|\[|\]|,|~"
t_VARIABLE_NAME = r"[a-zA-Z_][a-zA-Z0-9_]*"
t_INTEGER = r"0|[1-9]{1}[0-9]*"

# tokens to ignore
t_ignore = " \r\n\t\f"


class PlyLexer(Lexer):
    """
    Лексер-обертка сторонней библиотеки ply
    """
    def __init__(self) -> None:
        self.lexer = ply.lex.lex()

    def get_tokens(self, data: str) -> list:
        self.lexer.input(data)
        result = []

        while True:
            token = self.lexer.token()
            if not token:
                break
            result.append(Token(token.type, token.value))
        return result

    @staticmethod
    def tokenize(token: Token) -> str:
        if token.type == "VARIABLE_NAME":
            return "a"
        elif token.type == "INTEGER":
            return "k"
        elif token.type == "KEYWORD":
            return token.value
        else:
            raise ValueError(f"Unexpected token type {token}")
