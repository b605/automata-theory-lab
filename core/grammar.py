class Grammar:
    """Объект грамматики, для удобной передачи параметров в различные методы"""

    def __init__(
        self,
        noterminals: list,
        terminals: list,
        rules: dict,
        empty_str: str,
        start_nt: str,
        end_t: str,
    ) -> None:
        self.noterminals = noterminals
        self.terminals = terminals
        self.rules = rules
        self.empty = empty_str
        self.start = start_nt
        self.end_t = end_t
        self.table = {}

    @staticmethod
    def parse_rules(text: str, start_nt: str, empty_str: str = "h", end_t: str = "~"):
        rules = text.split("\n")

        result = {}
        noterminals = []

        for rule in rules:

            if "->" not in rule:
                break
            left, right = rule.split("->")
            right_many = right.split(" | ")

            if left not in result:
                result[left] = []

            noterminals.append(left)
            for right_str in right_many:
                elements = right_str.split(" ")
                result[left].append(elements)

        ts = {}
        for _, rule in result.items():
            for sr in rule:
                for token in sr:
                    if token not in result and token not in ts:
                        ts[token] = 1

        terminals = list(ts.keys())

        return Grammar(noterminals, terminals, result, empty_str, start_nt, end_t)

    @staticmethod
    def parse_from_file(
        filepath: str, start_nt: str, empty_str: str = "h", end_t: str = "~"
    ):

        # TODO: think about Exceptions
        with open(filepath, "r") as f:
            text = f.read()

        return Grammar.parse_rules(text, start_nt, empty_str, end_t)

    def generate_table(self) -> dict:

        # Добавим конечный терминал в список терминалов (до генерации таблицы!)
        self.terminals.append(self.end_t)

        for noterminal in self.noterminals:
            self.table[noterminal] = {}
            with_empty = False
            for rule in self.rules[noterminal]:

                if rule[0] != self.empty:
                    self.table[noterminal][rule[0]] = rule
                else:
                    with_empty = True

            # если в правилах порождения от текущего нетерминала есть пустой символ
            # То пропущенные терминалы в таблице такого нетерминала ставим
            # пустой символ
            if with_empty:
                for terminal in self.terminals:
                    if terminal not in self.table[noterminal]:
                        self.table[noterminal][terminal] = self.empty

        return self.table
