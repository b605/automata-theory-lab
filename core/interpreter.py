from typing import List, Tuple, Union
from core.analyzer import Analyzer
from core.exc import InterpreterException
from core.grammar import Grammar
from core.components import (
    ArrayReference,
    Constant,
    Action,
    IntegerVariable,
    Operand,
    Point,
    TempValue,
)
from core.lexer import Lexer, PlyLexer


class Interpreter:
    def __init__(self, grammar: Grammar, rpn_table: dict, lexer: Lexer = PlyLexer) -> None:
        self.analyzer = Analyzer(grammar, rpn_table, lexer)
        self.stack: List[Operand] = list()

    def _get2(self) -> Tuple[Operand, Operand]:
        """Метод достает два значения из стека

        :return: два верхних операнда из стека
        """
        x = self.stack.pop(-1)
        y = self.stack.pop(-1)
        return x, y

    def _get1(self) -> Operand:
        """Метод достает одно значение из стека

        :return: верхний операнд в стеке
        """
        x = self.stack.pop(-1)
        return x

    def _put(self, operand: Operand):
        """Метод кладет операнд в стек

        :param operand: Операнд
        """
        self.stack.append(operand)

    def _unpack_value(self, operand: Operand) -> Union[int, float]:
        """Распаковывает операнд, выдавая целочисленное значение

        :param operand: Операнд
        :raises InterpreterException: Cannot get value from an undefined array reference
        :raises InterpreterException: Cannot get value from an undefined integer variable
        :raises InterpreterException: Value can not be unpacked
        :return: Значение распаковки
        """
        if isinstance(operand, TempValue):
            return operand.get_value()
        elif isinstance(operand, ArrayReference):
            value = operand.get_value(self.analyzer.rpn.variables)
            if value is not None:
                return value
            raise InterpreterException(
                f"Cannot get value from an undefined array reference: {operand}"
            )
        elif isinstance(operand, IntegerVariable):
            value = operand.get_value(self.analyzer.rpn.variables)
            if value is not None:
                return value
            raise InterpreterException(
                f"Cannot get value from an undefined integer variable: {operand}"
            )
        elif isinstance(operand, Point):
            return operand.get_value()
        elif isinstance(operand, Constant):
            return operand.get_value(self.analyzer.rpn.consts)

        raise InterpreterException(f"Cannot be unpacked: {operand}")

    def run(self, code_input: str, in_data: list = None, to_stdout: bool = True) -> list:
        """Запуск работы интерпретатора

        :param code_input: Код программы
        :param in_data: Список входных значений программы, defaults to None
        :param to_stdout: Флаг вывода в STDOUT, defaults to True
        :raises InterpreterException: Unavailiable type to read to
        :raises InterpreterException: Index is out of range
        :raises InterpreterException: Can't assign value
        :raises InterpreterException: Negative size of array
        :raises InterpreterException: Reallocation of array variable
        :raises InterpreterException: Zero Division Error
        :raises InterpreterException: Can't get index with float number
        :raises InterpreterException: Negative sqrt argument
        :return: Список выведенных значений программы
        """

        self.stack = list()

        self.analyzer.run(code_input)

        generator = self.analyzer.rpn

        interpreter_pos = 0
        output_data = []
        input_pos = 0 if in_data else None
        while interpreter_pos < len(generator.result):

            element = generator.result[interpreter_pos]
            interpreter_pos += 1

            if isinstance(element.value, Operand):
                self.stack.append(element.value)

            else:
                if element.value == Action.unconditional_jump:
                    pos = self._get1()
                    interpreter_pos = pos.value

                elif element.value == Action.jump:
                    pos, condition_val = self._get2()

                    if self._unpack_value(condition_val) <= 0:
                        interpreter_pos = pos.value

                elif element.value == Action.eq:
                    b, a = self._get2()
                    res = 1 if self._unpack_value(a) == self._unpack_value(b) else 0
                    self._put(TempValue(value=res))

                elif element.value == Action.neq:
                    b, a = self._get2()
                    res = 1 if self._unpack_value(a) != self._unpack_value(b) else 0
                    self._put(TempValue(value=res))

                elif element.value == Action.gt:
                    b, a = self._get2()
                    res = 1 if self._unpack_value(a) > self._unpack_value(b) else 0
                    self._put(TempValue(value=res))

                elif element.value == Action.ge:
                    b, a = self._get2()
                    res = 1 if self._unpack_value(a) >= self._unpack_value(b) else 0
                    self._put(TempValue(value=res))

                elif element.value == Action.lt:
                    b, a = self._get2()
                    res = 1 if self._unpack_value(a) < self._unpack_value(b) else 0
                    self._put(TempValue(value=res))

                elif element.value == Action.le:
                    b, a = self._get2()
                    res = 1 if self._unpack_value(a) <= self._unpack_value(b) else 0
                    self._put(TempValue(value=res))

                elif element.value == Action.assign:
                    value, variable = self._get2()

                    if isinstance(variable, ArrayReference):
                        variable.set_value(generator.variables, self._unpack_value(value))

                    elif isinstance(variable, IntegerVariable):
                        variable.set_value(generator.variables, self._unpack_value(value))
                    else:
                        raise InterpreterException(f"Can't assign value: {value} to {variable}")

                elif element.value == Action.index:
                    index_op, array = self._get2()

                    index_value = self._unpack_value(index_op)
                    if isinstance(index_value, float):
                        raise InterpreterException(
                            "Can't get index with float number: {index_value}"
                        )

                    reference = array.get_array_ref(generator.variables, index_value)

                    if not reference:
                        raise InterpreterException(
                            f"Index is out of range: {self._unpack_value(index_value)}"
                        )

                    self._put(reference)

                elif element.value == Action.make:
                    size_value, array = self._get2()
                    if array.length(generator.variables) is None:
                        size = self._unpack_value(size_value)
                        if size <= 0:
                            raise InterpreterException(f"Negative size: {size}")

                        array.make(generator.variables, size)
                    else:
                        raise InterpreterException(f"Reallocation of array variable: {array.value}")

                elif element.value == Action.read:
                    variable = self._get1()

                    if input_pos is None:
                        data_input = int(input("IN:"))
                    else:
                        data_input = int(in_data[input_pos])
                        input_pos += 1

                    if isinstance(variable, ArrayReference):
                        variable.set_value(generator.variables, data_input)

                    elif isinstance(variable, IntegerVariable):
                        variable.set_value(generator.variables, data_input)

                    else:
                        raise InterpreterException(f"Unavailiable type to read to: {variable}")

                elif element.value == Action.write:
                    value = self._get1()
                    if to_stdout:
                        print(f"OUT: {self._unpack_value(value)}")
                    else:
                        output_data.append(self._unpack_value(value))

                elif element.value == Action.abs:
                    a = self._get1()
                    self._put(TempValue(value=abs(self._unpack_value(a))))

                elif element.value == Action.sqrt:
                    a = self._get1()
                    if self._unpack_value(a) < 0:
                        raise InterpreterException(
                            f"Negative sqrt argument! {self._unpack_value(a)}"
                        )
                    result = int(self._unpack_value(a) ** 0.5)
                    self._put(TempValue(value=result))

                elif element.value == Action.plus:
                    a, b = self._get2()
                    self._put(TempValue(value=self._unpack_value(a) + self._unpack_value(b)))

                elif element.value == Action.minus:
                    a, b = self._get2()
                    self._put(TempValue(value=self._unpack_value(b) - self._unpack_value(a)))

                elif element.value == Action.multiply:
                    a, b = self._get2()
                    self._put(TempValue(value=self._unpack_value(a) * self._unpack_value(b)))

                elif element.value == Action.divide:
                    a, b = self._get2()

                    if self._unpack_value(a) == 0:
                        raise InterpreterException(
                            f"Zero division Error! {self._unpack_value(b)} / {self._unpack_value(a)}"
                        )
                    self._put(TempValue(value=self._unpack_value(b) / self._unpack_value(a)))

                elif element.value == Action.negate:
                    a = self._get1()
                    self._put(TempValue(value=-self._unpack_value(a)))

        return output_data
