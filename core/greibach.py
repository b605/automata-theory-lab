from functools import lru_cache
from string import ascii_uppercase
from typing import List

from grammar import Grammar


class Greibach:
    @staticmethod
    def convert(grammar: Grammar) -> Grammar:

        # Получим грамматику без левой рекурсии и обновим список нетерминалов.
        # TODO: Необходимо реализовать метод для однозначности правила из
        # одного нетерминала (для массива и матрицы по необходимости)
        grammar = Greibach.remove_left_recursion(grammar)

        # Необходимо заменить правила,
        # где правая часть начинается с нетерминала на множество правил, порождаемых этим нетерминалом.
        # Повторять пока не получится нестрогая форма Грейбаха

        # Добавим вспомогательную функцию с рекурсивным вызовом
        @lru_cache(maxsize=1024)
        def unpack(noterminal: str) -> List[List[str]]:
            new_rules = []

            for rule in grammar.rules[noterminal]:
                if rule[0] in grammar.rules:
                    unp = unpack(rule[0])
                    for u in unp:
                        new_rules.append(u + rule[1:])
                else:
                    new_rules.append(rule)

            grammar.rules[noterminal] = new_rules
            return new_rules

        for nt in grammar.noterminals:
            unpack(nt)

        return grammar

    @staticmethod
    def remove_left_recursion(grammar: Grammar) -> Grammar:

        # получим неиспользуемые заглавные буквы для новых нетерминалов
        unused = set(ascii_uppercase).difference(grammar.noterminals)

        old_rules = dict(grammar.rules)

        for left, many_right in old_rules.items():

            to_move = []
            saved = None
            for rule in many_right:
                # если True, то имеет место левая рекурсия
                if rule[0] != left:
                    if not saved:
                        saved = rule[0]
                else:
                    to_move.append(rule[1:])

            if not to_move:
                continue

            # TODO: use unused Uppercase letters instead of this
            new_noterminal = unused.pop()

            grammar.noterminals.append(new_noterminal)

            to_replace = [[saved, new_noterminal]]

            for i in range(len(to_move)):
                to_move[i].append(new_noterminal)

            to_move.append([grammar.empty])

            grammar.rules[left] = to_replace
            grammar.rules[new_noterminal] = to_move

        return grammar

    @staticmethod
    def write_file(grammar: Grammar, filepath: str):

        with open(filepath, "w") as f:
            for left, many_right in grammar.rules.items():
                line = f"{left}->{' | '.join(list(map(lambda x: ' '.join(x), many_right)))}\n"
                f.write(line)
