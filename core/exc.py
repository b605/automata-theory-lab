"""
Exception definitions
"""


class AnalyzerException(Exception):
    def __init__(self, message: str) -> None:
        self.prefix = "AnalyzerException: "
        super().__init__(self.prefix + message)


class GeneratorException(Exception):
    def __init__(self, message: str) -> None:
        self.prefix = "GeneratorException: "
        super().__init__(self.prefix + message)


class InterpreterException(Exception):
    def __init__(self, message: str) -> None:
        self.prefix = "InterpreterException: "
        super().__init__(self.prefix + message)


class LexerException(Exception):
    def __init__(self, message: str) -> None:
        self.prefix = "LexerException: "
        super().__init__(self.prefix + message)
