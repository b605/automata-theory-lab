from abc import abstractmethod
from enum import Enum
from typing import List
from core.exc import GeneratorException
from core.grammar import Grammar
from core.lexer import Token
from core.components import (
    Action,
    ArrayVariable,
    Element,
    IntegerVariable,
    Constant,
    CONVERT_TO_INTERPRETER_ACTION,
    Point,
)


class TableRPN:
    """
    TableRPN class содержит статические методы для работы с ОПС семантикой
    """

    @staticmethod
    def from_file(grammar: Grammar, filepath: str) -> dict:
        """Читает семантическую грамматику из файла

        :param grammar: Грамматика для таблицы
        :param filepath: [Путь семантической грамматики для ОПС
        :return: таблица семантических правил
        """

        with open(filepath, "r") as f:
            text = f.read()

        rules = text.split("\n")

        rpn_table = {}

        for rule in rules:

            if "->" not in rule:
                break

            left, right = rule.split("->")
            right_rpn = right.split(" | ")

            # case if left rule is not unique on left side of '->'
            # TODO: can be removed with careful
            if left not in rpn_table:
                rpn_table[left] = {}

            for i in range(len(right_rpn)):
                tokens = right_rpn[i].split(" ")
                if tokens[0] == grammar.empty:
                    continue
                rpn_table[left][grammar.rules[left][i][0]] = tokens

        return rpn_table


UNHANDLED_ACTIONS = [
    "m",
    "w",
    "r",
    "i",
    "+",
    "-",
    "/",
    "*",
    ">",
    ">=",
    "<",
    "<=",
    "==",
    "!=",
    "neg",
    "abs",
    "sqrt",
]


class SemanticAction(Enum):
    decl_int = "int"
    decl_array = "array"
    begin = "v1"
    end = "v2"
    unique_assertion = "v3"
    t1 = "t1"
    t2 = "t2"
    t3 = "t3"
    t4 = "t4"
    t5 = "t5"
    variable = "a"
    const = "k"
    empty = "e"
    unhandled = None


CONVERT_HANDLED_TO_ENUM = {
    "int": SemanticAction.decl_int,
    "array": SemanticAction.decl_array,
    "v1": SemanticAction.begin,
    "v2": SemanticAction.end,
    "v3": SemanticAction.unique_assertion,
    "t1": SemanticAction.t1,
    "t2": SemanticAction.t2,
    "t3": SemanticAction.t3,
    "t4": SemanticAction.t4,
    "t5": SemanticAction.t5,
    "a": SemanticAction.variable,
    "k": SemanticAction.const,
    "e": SemanticAction.empty,
}

STARTUP_RPN_STACK = [SemanticAction.empty, SemanticAction.empty]


class Generator:
    """
    Generator class реализует генерацию ОПС строки, магазин меток и таблицы переменных и констант
    для последующей интерпретации
    """

    def __init__(self, grammar: Grammar, rpn_table: dict) -> None:
        self.grammar = grammar
        self.rpn = rpn_table
        self._unhandled_stack: List[str] = list()
        self._stack: List[SemanticAction] = list(STARTUP_RPN_STACK)
        self.result: List[Element] = list()
        self._points = list()
        self._k = None
        self.consts: List[int] = list()
        self._var_cursor = None
        self.variables = {"int": {}, "array": {}}

    def reset(self):
        """
        Сбрасывает результат и состояние генератора
        :return: None
        """
        self._unhandled_stack = []
        self._stack = list(STARTUP_RPN_STACK)
        self.result = []
        self._points = []
        self._k = 0
        self.consts = []
        self._var_cursor = None
        self.variables = {"int": {}, "array": {}}

    def push(self, noterminal: str, terminal: str):
        """
        метод кладет в стэк семантические правила, полученные из таблицы ОПС правил
        :param noterminal: нетерминал
        :param terminal: терминал
        :return: None
        """

        to_stack = []
        for action in self.rpn[noterminal][terminal]:
            if action in CONVERT_HANDLED_TO_ENUM:
                to_stack.append(CONVERT_HANDLED_TO_ENUM[action])
            else:
                self._unhandled_stack.append(action)
                to_stack.append(SemanticAction.unhandled)

        self._stack = to_stack + self._stack

    def _handle_unhandled(self):
        """
        Добавляет в ОПС строку Элемент действие для интерпретации
        """
        self.result.append(
            Element(value=CONVERT_TO_INTERPRETER_ACTION[self._unhandled_stack.pop()])
        )

    def _handle(self, element: SemanticAction, token: Token):
        """
        Выполняет обработку комплексного семантического действия, некоторые действия требуют счётчика
        текущего генерируемого элемента

        :param element: Семантическое действие
        :param token: Текущий токен
        :raises GeneratorException: Not a variable
        :raises GeneratorException: Used undefined variable
        :raises GeneratorException: Not an integer value
        :raises GeneratorException: Repeating declaration
        """

        if element == SemanticAction.variable:

            if token.type != "VARIABLE_NAME":
                raise GeneratorException(self._generate_error("Not a variable", token))

            elif (
                token.value not in self.variables["int"]
                and token.value not in self.variables["array"]
            ):
                raise GeneratorException(
                    self._generate_error("Used undefined variable", token)
                )
            if token.value in self.variables["int"]:
                self.result.append(Element(value=IntegerVariable(value=token.value)))
            else:
                self.result.append(Element(value=ArrayVariable(value=token.value)))

        elif element == SemanticAction.const:
            if token.type != "INTEGER":
                raise GeneratorException(
                    self._generate_error("Not an Integer value", token)
                )

            self.result.append(Element(value=Constant(value=len(self.consts))))
            self.consts.append(int(token.value))

        elif element == SemanticAction.decl_int:
            self._var_cursor = "int"

        elif element == SemanticAction.decl_array:
            self._var_cursor = "array"

        elif element == SemanticAction.unique_assertion:
            # Asserting unique variable name declaration
            if (
                token.value in self.variables["int"]
                and token.value in self.variables["array"]
            ):
                raise GeneratorException(
                    self._generate_error("Repeating declaration, same names", token)
                )
            else:
                self.variables[self._var_cursor][token.value] = None

        elif element == SemanticAction.t1:
            self._points.append(self._k)
            self.result.append(None)
            self.result.append(Element(value=Action.jump))

        elif element == SemanticAction.t2:
            point = self._points.pop(-1)
            self.result[point] = Element(value=Point(value=self._k + 2))
            self._points.append(self._k)
            self.result.append(None)
            self.result.append(Element(value=Action.unconditional_jump))

        elif element == SemanticAction.t3:
            point = self._points.pop(-1)
            self.result[point] = Element(value=Point(value=self._k))

        elif element == SemanticAction.t4:
            self._points.append(self._k)

        elif element == SemanticAction.t5:
            point = self._points.pop(-1)
            self.result[point] = Element(value=Point(value=self._k + 2))
            point = self._points.pop(-1)
            self.result.append(Element(value=Point(value=point)))
            self.result.append(Element(value=Action.unconditional_jump))

    def remove(self, token: Token):
        """Удаляет верхний элемент стека генератора, выполняя семантическое действие
        Стоит отметить, что стек генератора имеет следующий тип: [SemanticAction(Enum)]

        :param token: Токен
        """

        element = self._stack.pop(0)

        if element == SemanticAction.empty:
            return

        elif element == SemanticAction.unhandled:
            self._handle_unhandled()

        else:
            self._handle(element, token)

        # changing current generating element number
        self._k += len(self.result) - self._k

    @property
    def stack(self) -> List[str]:
        """Возвращает список строк, представляющий стек ОПС генератора

        :return: список строк, стек генератора
        """

        generated_stack = []
        count = 0
        for element in self._stack:
            if element == SemanticAction.unhandled:
                generated_stack.append(self._unhandled_stack[count])
                count += 1
            else:
                generated_stack.append(element.value)

        return generated_stack

    def _generate_error(self, error: str, token: Token) -> str:
        """Метод для генерации строки сообщения об ошибки

        :param error: Сообщение об ошибке
        :param token: Еокен для дополнительной информации
        :return: Сообщение об ошибке
        """
        return f"{error} on {self._k}: {token.value} as {token.type}"
