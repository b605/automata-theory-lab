from collections import OrderedDict
from typing import List
import csv
from prettytable import PrettyTable
from abc import ABC, abstractmethod
from core.grammar import Grammar


class AnalysisTableGenerator:
    """
    Обёртка для PrettyTable. Автоматически добавляются шаги работы.
    Имена столбцов: ["step", "input", "stack", "rule", "generator", "rpn"]
    step - заполнять не нужно!
    """

    def __init__(self) -> None:
        self._field_names = ["step", "input", "stack", "rule", "generator", "rpn"]
        self._generator = PrettyTableGenerator(self._field_names)

        for name in self._generator._table.field_names[1:]:
            self._generator._table.align[name] = "l"

        self._step = 1
        self._current_data = OrderedDict()
        for field_name in self._generator.field_names:
            self._current_data[field_name] = None

        self._current_data["step"] = self._step

    def reset(self):
        self._step = 1
        self._current_data = OrderedDict()
        self._generator.reset()

    @property
    def row(self) -> OrderedDict:
        return self._current_data

    def save_row(self):
        row = []
        for _, val in self._current_data.items():
            row.append(val)

        self._generator.add_row(row)
        self._current_data.clear()
        self._step += 1
        self._current_data["step"] = self._step

    def write(self, filepath: str) -> None:
        self._generator.write(filepath)


class TableGenerator(ABC):
    def __init__(self, field_names: List[str], **kwargs) -> None:
        super().__init__()

    @abstractmethod
    def add_row(self, row: List[str]) -> None:
        pass

    @abstractmethod
    def write(self, filepath: str) -> None:
        pass


class PrettyTableGenerator(TableGenerator):
    def __init__(self, field_names: List[str]) -> None:
        self._table = PrettyTable()
        self._table.field_names = field_names

    @property
    def field_names(self):
        return self._table.field_names

    def add_row(self, row: List[str]) -> None:
        return self._table.add_row(row)

    def write(self, filepath: str) -> None:
        with open(filepath, "w") as file:
            file.write(self._table.__str__())

    def reset(self):
        self._table.clear_rows()


class CSVTableGenerator(TableGenerator):
    def __init__(self, field_names: List[str]) -> None:
        self._field_names = field_names
        self._rows = list()

    def add_row(self, row: List[str]) -> None:
        self._rows.append(row)

    def write(self, filepath: str) -> None:
        with open(filepath, "w") as file:
            writer = csv.writer(file, delimiter=",")
            writer.writerow(self._field_names)
            writer.writerows(self._rows)


def print_table_ll(
    filepath: str,
    grammar: Grammar,
    delimiter: str = "",
    table_generator=PrettyTableGenerator,
):
    """Печатает таблицу правил в файл.В списке терминалов необходим терминал конца кода, для корректности

    Args:
        filepath (str):
        grammar (Grammar):
        delimiter (str, optional):
        table_generator ([type], optional): Defaults to PrettyTableGenerator.
    """

    t_no_empty = list(grammar.terminals)
    t_no_empty.remove(grammar.empty)

    tb = table_generator([" "] + t_no_empty)

    for noterminal in grammar.noterminals:
        row = list()
        row.append(noterminal)
        for terminal in t_no_empty:
            if terminal in grammar.table[noterminal]:
                row.append(delimiter.join(grammar.table[noterminal][terminal]))
            else:
                row.append(" ")
        tb.add_row(row)

    tb.write(filepath)


def print_table_rpn(
    filepath,
    rpn_table: dict,
    grammar: Grammar,
    delimiter=" ",
    table_generator=PrettyTableGenerator,
):
    """Печатает таблицу семантический ОПС правил в файл

    Args:
        filepath ([type]):
        rpn_table (dict):
        grammar (Grammar):
        delimiter (str, optional): Defaults to " ".
        table_generator ([type], optional): Defaults to PrettyTableGenerator.
    """

    t_no_empty = list(grammar.terminals)
    t_no_empty.remove(grammar.empty)

    tb = table_generator([" "] + t_no_empty)

    for noterminal in grammar.noterminals:
        row = list()
        row.append(noterminal)

        for terminal in t_no_empty:
            if terminal in rpn_table[noterminal]:
                row.append(delimiter.join(rpn_table[noterminal][terminal]))
            else:
                row.append(" ")
        tb.add_row(row)

    tb.write(filepath)
