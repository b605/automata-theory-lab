from settings import Config
from core.exc import AnalyzerException
from core.grammar import Grammar
from core.lexer import Lexer
from core.rpn import Generator
from core.utility import AnalysisTableGenerator


class Analyzer:
    """
    Analyzer работает синхронно с генератором ОПС (свойство объекта - Generator)
    Единственный метод run позволяет запустить процесс генерации ОПС строки из текстового кода.
    Также использует Лексический токенизатор от пакета ply
    """

    def __init__(self, grammar: Grammar, rpn_table: dict, lexer: Lexer) -> None:
        """Конструктор анализатора

        :param grammar: Грамматика языка
        :param rpn_table: RPN таблица, для генератора
        """

        # Подготовим проверку за O(1) на тип значения в стеке
        # (терминал - 0,нетерминал - 1)
        self.stack_types = {}
        for terminal in grammar.terminals:
            self.stack_types[terminal] = 0
        for noterminal in grammar.noterminals:
            self.stack_types[noterminal] = 1

        self.grammar = grammar
        self.rpn = Generator(self.grammar, rpn_table)
        self.lexer = lexer()

    def run(
        self,
        code_input: str,
        filepath: str = Config.OUTPUT_DIR / f"analytics_default",
    ) -> None:
        """Запуск работы анализатора

        :param code_input:
        :param filepath: defaults to Config.OUTPUT_ANALYTICS
        :param lexer: defaults to PlyLexer

        :raises AnalyzerException: Token doesn't match any of possible rules
        :raises AnalyzerException: Expected different token
        :raises AnalyzerException: Undefined Behavior
        :raises AnalyzerException: Stack is not empty on finish
        """

        # Сбрасываем магазин ОПС генератора и кладем два пустых элемента
        self.rpn.reset()

        code_input += self.grammar.end_t

        # Создадим магазин и положим в него терминал конца кода и начальный
        # нетерминал
        stack = [self.grammar.start, self.grammar.end_t]

        # tokens: List[Tuple[str, str]]
        # [(type, value)]
        tokens = self.lexer.get_tokens(code_input)

        # Инициализация таблицы для вывода в файл - filepath
        table = AnalysisTableGenerator()

        # position of real element
        pos = 0
        # Главный цикл анализатора
        while tokens:

            # верхний токен в коде
            # Token(type, value)
            raw_token = tokens[0]

            # Ряд для генерации таблицы ходов анализатора=
            table.row["input"] = " ".join(list(map(lambda v: v.value, tokens)))
            table.row["stack"] = " ".join(stack)

            # верхний элемент магазина
            top_stack_element = stack[0]

            # Преобразуем имя, значение в терминальный символ
            token = self.lexer.tokenize(raw_token)

            # Верхний элемент стека - нетерминал
            if self.stack_types[top_stack_element] == 1:
                if token in self.grammar.table[top_stack_element]:

                    # Добавляем правило вывода
                    table.row[
                        "rule"
                    ] = f"{top_stack_element} -> {''.join(self.grammar.table[top_stack_element][token])}"

                    if self.grammar.table[top_stack_element][token] != self.grammar.empty:
                        stack.pop(0)
                        stack = self.grammar.table[top_stack_element][token] + stack

                        # RPN generator
                        self.rpn.remove(raw_token)
                        self.rpn.push(top_stack_element, token)

                    else:
                        stack.pop(0)
                        self.rpn.remove(raw_token)

                    table.row["generator"] = " ".join(self.rpn.stack)
                    table.row["rpn"] = " ".join(map(str, self.rpn.result))
                else:
                    raise AnalyzerException(
                        f"{token} should match one of this rules: "
                        f"{self.grammar.table[top_stack_element]}"
                    )

            # Верхний элемент стека - терминал
            elif self.stack_types[top_stack_element] == 0:
                if top_stack_element == token:
                    stack.pop(0)
                    tokens.pop(0)

                    # RPN generator
                    self.rpn.remove(raw_token)
                    pos += 1

                    # На этом шаге правило перехода не используется
                    # записывается пустая строка в столбец rule таблицы
                    table.row["rule"] = ""
                    table.row["generator"] = " ".join(self.rpn.stack)
                    table.row["rpn"] = " ".join(map(str, self.rpn.result))
                else:
                    raise AnalyzerException(f"Expected {top_stack_element} got {token}")
            else:
                raise AnalyzerException(f"Unexpected behavior")

            # добавляем в таблицу результат работы на i-м шаге
            table.save_row()

        table.write(filepath=filepath)

        if len(stack) != 0:
            raise AnalyzerException(f"Unexpected behavior. Stack must be empty: {stack}")
